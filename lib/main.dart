import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() => runApp(myApp());

class myApp extends StatelessWidget{
  const myApp();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LogoApp(),
    );
  }

}
class AnimatedLogo extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
          child: Opacity(
            opacity: _opacityTween.evaluate(animation),
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              height: _sizeTween.evaluate(animation),
              width: _sizeTween.evaluate(animation),
              child: FlutterLogo(),
      ),
          ),
        );
  }
}

class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

   @override
  void initState() {
     super.initState();
     controller =
         AnimationController(duration: const Duration(seconds: 2), vsync: this);
     animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
       ..addListener(() {
         setState(() {

         });
       });
   }

  @override
 // Widget build(BuildContext context) => AnimatedLogo(animation: animation);
Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
        appBar: AppBar(
          title:
          Icon(Icons.home),
          backgroundColor: Colors.deepOrangeAccent,
    ),
          body: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
              ConstrainedBox(
                 constraints: BoxConstraints(maxWidth: 200),
                child: AnimatedLogo(animation: animation),
              ),
                RaisedButton(
                  color: Colors.blueAccent,
                    child:
                    Text('ANIMATE',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black87),
                    ),
                  onPressed: () {
                    if (controller.status == AnimationStatus.completed) {
                            controller.reverse();
                    } else {
                            controller.forward();
                           }
                  },// onPressed

                ),
              // whoMadeThis(),
              ],
     ),
     ),
    );
  }
   @override
  void dispose() {
     controller.dispose();
     super.dispose();
   }
}

Widget whoMadeThis() => Container(
  height: 30,
  width: 300,
  child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text('CSUSM: Fall 2020',
      style: TextStyle(
        fontWeight: FontWeight.normal,
       color: Colors.red.withOpacity(0.5),

      ),
      ),
      Text('CS 481 Mobile Programming',
        style: TextStyle(
          fontWeight: FontWeight.normal,
          color: Colors.lightGreen.withOpacity(0.5),

        ),
      ),
      Text('Damian Salazar',
        style: TextStyle(
          fontWeight: FontWeight.normal,
          color: Colors.deepOrangeAccent.withOpacity(0.5),

        ),
      ),
    ],
  ),
);